"""
Copyright Dejan Kostyszyn 2019

This method prints all key paths that are read
during reading the data. This method only exists
for controlling the functionality of the
ReadData.py
"""

import ReadData as rd
from options.test_data_reading_options import ReadOptions
import time, utils
from os.path import join
import numpy as np

def print_data_paths():
    start_time = time.time()

    opt = ReadOptions().parse()
    vl = rd.ValLoader(opt)
    pet_file_paths, cancer_lesionment_paths, patient_ids, prostate_contour_paths = vl.read_data_paths()
    results_path = opt.results_path
    utils.create_folder(results_path)

    patients_with_multi_lesions = 0

    # Create file.
    filename = join(results_path, "data_paths.txt")
    with open(filename, 'w') as f:
        f.write("id folder nr_of_lesions:\n")

    msg = ""

    # Read patient ids and folders.
    print("Patient id: patient folder")
    for key, value in patient_ids.items():
        nr_of_lesions = len(cancer_lesionment_paths[value])
        if nr_of_lesions >= 2:
            patients_with_multi_lesions += 1
        msg += "{} {} {}\n".format(key, value, nr_of_lesions)
    print(msg)

    # Write into file.
    with open(filename, 'a') as f:
        f.write(msg)


    # Read PET file paths.
    msg = ""
    print("PET file paths")
    for key, value in pet_file_paths.items():
        msg += "{}: {}\n".format(key, value)
    print(msg)

    # Write into file.
    with open(filename, 'a') as f:
        f.write(msg)

    # Read cancer lesion paths.
    msg = ""
    print("Cancer lesion paths")
    for key, value in cancer_lesionment_paths.items():
        msg += "{}: {}\n".format(key, value)
    print(msg)

    # Write into file.
    with open(filename, 'a') as f:
        f.write(msg)

    # Read prostate contour paths.
    msg = ""
    print("prostate contour paths")
    for key, value in prostate_contour_paths.items():
        msg += "{}: {}\n".format(key, value)
    print(msg)

    msg += "There are {} patiens with multiple lesions and {} with only one.".format(
        patients_with_multi_lesions, len(patient_ids) - patients_with_multi_lesions)

    # Write into file.
    with open(filename, 'a') as f:
        f.write(msg)

    

    end_time = time.time()

    print("Completed in {}s".format(end_time - start_time))

if __name__ == "__main__":
    print_data_paths()