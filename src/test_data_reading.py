import ReadData as rd
from options.train_options import TrainOptions
import time, utils, sys
import numpy as np
from termcolor import colored

def test_data_reading():
    start_time = time.time()

    opt = TrainOptions().parse()
    opt.shuffle = "False"
    vl = rd.ValLoader(opt)
    max_idx = vl.nr_of_patients()

    # Shuffle data reading.
    idx_train, idx_val, idx_test = utils.divide_data(max_idx, opt)
    idx_list = np.concatenate((idx_train, idx_val, idx_test), 0)

    print("{} patients found. Started reading data in order {}".format(max_idx, idx_list), end="\n\n")

    for idx in idx_list:
        data, cancer_seg, p_id, _, ratio, _ = vl.val_loader(idx=idx, is_train=False)
        pet = data[0]
        prostate = data[1]
        print("patient index = {}".format(idx))
        print("patient id = {}".format(p_id))
        print("pet file shape = {}".format(pet.shape))
        print("cancer lesions file shape = {}".format(cancer_seg.shape))
        print("prostate contour file shape = {}".format(prostate.shape))
        print("")

        if 0 == np.count_nonzero(pet):
            print(colored("PET file of patient {} is empty.".format(p_id), "red"))
        if 0 == np.count_nonzero(cancer_seg):
            print(colored("Cancer lesions file of patient {} is empty.".format(p_id), "red"))
        if 0 == np.count_nonzero(prostate):
            print(colored("Prostate contour file of patient {} is empty.".format(p_id), "red"))

        idx += 1

    end_time = time.time()
    print("Test successful. Completed in {}s".format(end_time - start_time))
    
if __name__ == "__main__":
    test_data_reading()
