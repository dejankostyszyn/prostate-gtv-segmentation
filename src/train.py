"""
Copyright Dejan Kostyszyn 2019

This method trains the network.
"""

import ReadData as rd
import numpy as np
from matplotlib import colors
import torch, os, nrrd, time
from model.UNet import UNet
import matplotlib.pyplot as plt
from options.train_options import TrainOptions
import utils, csv, sys
from os.path import join

def train():
    print("initializing...")
    start_time = time.time()
    iters = 0

    # Initializing.
    opt = TrainOptions().parse()
    vl = rd.ValLoader(opt)

    max_idx = vl.nr_of_patients()
    nr_of_epochs = opt.training_epochs
    results_path = join(opt.results_path)
    trained_model_name = opt.trained_model
    final_model_name = join(results_path, trained_model_name)

    # Check if results folder exists and if so, ask user if really want to continue.
    utils.overwrite_request(results_path)

    # Help variable for correct shaping of large tensors.
    old_shape = (0,0,0)
    new_shape = (128,128, 128)

    # Create folders and files.
    if opt.epoch == 0:
        utils.create_folder(results_path)
        with open(results_path + "/training_results.csv", "w", newline="") as file:
                writer = csv.writer(file, delimiter=",", quotechar='|', quoting=csv.QUOTE_MINIMAL)
                writer.writerow(["epoch", "iterations", opt.loss_fn+" loss", "img idx", "patient id"])
        with open(results_path + "/validation_results.csv", "w", newline="") as file:
                writer = csv.writer(file, delimiter=",", quotechar='|', quoting=csv.QUOTE_MINIMAL)
                writer.writerow(["epoch", "iterations", "patient idx", opt.loss_fn+" loss", "dice coefficient",\
                    "hausdorff distance", "average surface distance metric",\
                    "average symmetric surface distance", "precison", "sensitivity",\
                    "specificity", "patient id"])

    # Write options into a file.
    with open(results_path + '/training_options.txt', 'w') as f:
        f.write(" ".join(sys.argv[1:]))

    num_classes = 2

    overall_train_loss = []
    overall_val_loss = [[],[],[],[],[],[],[],[]]
    overall_CEL = []
    
    # Divide data into training, validation and test set.
    idx_train, idx_val, idx_test = utils.divide_data(max_idx, opt)

    # Insert manual training, validation and test permutation here if you wish.
    # idx_train = [72,60,132,71,31,101,144,158,96,166,164,161,125,56,124,93,121,70,45,57,122,29,77,49,90,82,48,159,66,128,38,150,58,171,44,163,120,22,21,30,137,33,52,138,140,102,80,106,94,110,105,34,97,88,160,41,165,154,111,113,67,89,61,115,112,26,63,119,149,155,157,73,51,130,116,174,156,133,143,98,95,39,131,50,126,100,85,59,43,167,135,134,141,142,74,91,87,68,145,78,170,108,25,62,103,153,69,76,40,139,129,117,81,86,84,65,118,92,55,37,99,83,32,0,169,79,19,148,136,35,36,104,23,168,42,173,123,127,162,109,172,54,107,147,146]
    # idx_val = [53,27,75,151,152,2,24,114,64,47,28,46]
    # idx_test = [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20]
    
    if not opt.no_data_info == True:
        print("# train images = {}, # val images = {}, # test images = {}".format(len(idx_train), len(idx_val), len(idx_test)), end="\n\n")
        print("train images = \t{}".format(idx_train))
        print("val images = \t{}".format(idx_val))
        print("test images = \t{}".format(idx_test))

    data, lesion, _, _, _, _ = vl.val_loader(is_train=False)
    model = UNet(data.shape, lesion.shape, num_classes, opt)  
    
    if opt.epoch > 0:
        # Load pretrained model.
        model.load_state_dict(torch.load(join(opt.results_path, opt.trained_model)))
        # Load previous statistics.
        with open(results_path + "/training_results.csv", newline="") as file:
            next(file)
            for row in file:
                w = row.split(",")
                iters = int(w[1])
                overall_train_loss.append(w[2])
        with open(results_path + "/validation_results.csv",  newline="") as file:
            next(file)
            for row in file:
                w = row.split(",")
                for i in range(8):
                    overall_val_loss[i].append(float(w[i+2]))


    elif opt.epoch < 0:
        sys.exit("{} is not a valid value as starting epoch.".format(opt.epoch))
    
    # Enabling GPU
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)

    # Set optimizer
    optimizer = utils.set_optimizer(opt, model_params=model.parameters())

    # Set loss function.
    loss_fn = utils.set_loss_fn(opt, num_classes, device)

    for epoch in range(opt.epoch, nr_of_epochs):
        # Perform training
        model.train()
        print("started training in epoch {} ...".format(epoch))
        train_losses = []
        
        for idx in idx_train:
            # Load data.
            data, lesion, p_id, _, ratio, _ = vl.val_loader(idx=idx)

            # Perform data augmentation.
            data, lesion = utils.data_augmentation(opt, data, lesion)
            lesion = lesion.to(device)
            data = data.to(device)

            # Computing loss.
            # Predict.
            y_pred = model(data.unsqueeze(0))
            if opt.loss_fn == "dice":
                loss = loss_fn(y_pred.squeeze(), lesion.float())
            else:
                loss = loss_fn(y_pred, lesion.long().unsqueeze(0))

            loss.requires_grad_(True)
            train_losses.append(loss.item())
            iters += 1

            print("epoch: {0}, loss: {1}, img idx: {2}, iterations: {3}, passed time: {4:.2f}s".format(
                    epoch, loss, idx, iters, time.time()- start_time))

            # Zero gradients, perform a backward pass, and update the weights.
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            with open(results_path + "/training_results.csv", "a", newline="") as file:
                writer = csv.writer(file, delimiter=",", quotechar='|', quoting=csv.QUOTE_MINIMAL)
                writer.writerow([epoch, iters, loss.item(), idx, p_id])

            # Intermediate saving of results.
            if epoch % opt.save_freq == 0 and epoch > 0:
                torch.save(model.state_dict(), final_model_name)

                intermediate_model_name = results_path + "/_" + str(epoch) + "_" + trained_model_name
                torch.save(model.state_dict(), intermediate_model_name)
            
        overall_train_loss.append(np.mean(train_losses))
        print("finished training for epoch {}...".format(epoch))

        # Perform validation.
        print("starting validation in epoch {} ...".format(epoch))
        model.eval()
        val_losses = [[],[],[],[],[],[],[],[]]
        CEL = []
        with torch.no_grad():
            for idx in idx_val:
                data, lesion, p_id, _, ratio, _ = vl.val_loader(idx=idx, is_train=False)
                if opt.augmentation in ["all", "scale"]:
                    data, lesion = utils.rescale(data, lesion)

                lesion = lesion.to(device)
                data = data.to(device)

                # Predict
                y_pred = model(data.unsqueeze(0))

                # Computing loss.
                if opt.loss_fn == "dice":
                    loss = loss_fn(y_pred.squeeze(), lesion.float())
                    loss_fn2 = torch.nn.CrossEntropyLoss()
                    CEL.append(loss_fn2(y_pred, lesion.long().unsqueeze(0)).item())
                else:
                    loss = loss_fn(y_pred, lesion.long().unsqueeze(0))
                l1 = loss.item()

                val_losses[0].append(l1)
                prediction = torch.argmax(y_pred.squeeze(0), dim=0).float()
                
                prediction = prediction.cpu().detach().numpy()
                
                val_losses = utils.compute_losses(prediction, lesion.cpu().detach().numpy(), val_losses)

                with open(results_path + "/validation_results.csv", "a", newline="") as file:
                    writer = csv.writer(file, delimiter=",", quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    writer.writerow([epoch, iters, idx, val_losses[0][-1], val_losses[1][-1], val_losses[2][-1],\
                        val_losses[3][-1], val_losses[4][-1], val_losses[5][-1], val_losses[6][-1], val_losses[7][-1], p_id])
                print("epoch: {}, iters: {}, loss: {}, DSC: {}".format(epoch, iters, val_losses[0][-1], val_losses[1][-1]))

        for i in range(8):
            overall_val_loss[i].append(np.mean(val_losses[i]))
        print("finished validation with mean loss = {}".format(np.mean(val_losses[0])))

        # Plotting train and val losses.
        if opt.loss_fn == "dice":
            overall_CEL.append(np.mean(CEL))
            utils.plot_losses(opt, results_path, "Training and validation losses", "epochs",\
            "loss", "dice_coefficient", \
            [overall_train_loss, "train ("+opt.loss_fn+")"],\
            [overall_val_loss[0], "val ("+opt.loss_fn+")"],\
            [overall_val_loss[1], "val (dice coeff)"],\
            [overall_CEL, "val (cross entropy)"])
            utils.plot_losses(opt, results_path, "Hausdorff distance", "epochs",\
                "loss", "hausdorff_distance.png", \
                [overall_val_loss[2], "val (hd)"],\
                [overall_val_loss[3], "val (asd)"],\
                [overall_val_loss[4], "val (assd)"])
            utils.plot_losses(opt, results_path, "Different metrics", "epochs",\
            "loss", "other_metrics.png",\
            [overall_val_loss[5], "val (precision)"],\
            [overall_val_loss[6], "val (sensitivity)"],\
            [overall_val_loss[7], "val (specificity)"])
        else:
            utils.plot_losses(opt, results_path, "Training and validation losses", "epochs",\
                "loss", "dice_coefficient", \
                [overall_train_loss, "train ("+opt.loss_fn+")"],\
                [overall_val_loss[0], "val ("+opt.loss_fn+")"],\
                [overall_val_loss[1], "val (dice coeff)"])
            utils.plot_losses(opt, results_path, "Hausdorff distance", "epochs",\
                "loss", "hausdorff_distance.png", \
                [overall_val_loss[2], "val (hd)"],\
                [overall_val_loss[3], "val (asd)"],\
                [overall_val_loss[4], "val (assd)"])
            utils.plot_losses(opt, results_path, "Different metrics", "epochs",\
                "loss", "other_metrics.png",\
                [overall_val_loss[5], "val (precision)"],\
                [overall_val_loss[6], "val (sensitivity)"],\
                [overall_val_loss[7], "val (specificity)"])

    # Save final model
    torch.save(model.state_dict(), final_model_name)
    end_time = time.time()
    print("Completed training and validation in {}s".format(end_time - start_time))

if __name__ == "__main__":
    train()