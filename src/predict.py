"""
Copyright Dejan Kostyszyn 2020

This method uses a trained network and predicts
the location and gleason score of prostate cancer
lesions for given PET data.
"""

import numpy as np
import torch, nrrd, os, sys
from model.UNet import UNet
from options.predict_options import PredictOptions
import utils, time
from scipy.ndimage import measurements
from os.path import join, split

# Standard deviation to manually change.
std = 4.060953698510707
# Arithmetic mean to manually change.
mean = 1.3583295982641057

def fit_prediction_to_pet(pet_shape, pet_h, pred, minx, maxx, miny, maxy, minz, maxz, opt):
    t = np.full(pet_shape, 0, dtype=float)

    t[minx:maxx, miny:maxy, minz:maxz] = pred

    return t

def predict():
  with torch.no_grad():
    # Initializing.
    opt = PredictOptions().parse()

    # Check if results folder exists and if so, ask user if really want to continue.
    results_path, _ = split(opt.prediction_name)
    utils.overwrite_request(results_path)
    utils.create_folder(results_path)
    
    num_classes = 2

    pet, pet_h = nrrd.read(opt.pet_file)
    pet = np.float32(pet)
    contour, cont_h = nrrd.read(opt.prostate_contour)
    contour = np.float32(contour)
    
    old_pet_shape = pet.shape

    pet, contour, minx, maxx, miny, maxy, minz, maxz = utils.fit_pet_shape(pet, contour)

    # Zero-mean normalization
    if opt.normalize.lower() == "global":
        pet = pet - mean
        pet = pet / std
    elif opt.normalize.lower() == "local":
        pet = (pet - pet.min()) / (pet.max() - pet.min())
    elif opt.normalize.lower() != "none":
        sys.exit("{} is not a valid value for normalize option. Choose one out of the set {None, local, global}".format(opt.normalize))

    # Concatenate PET and prostate contour.
    data = torch.cat((pet.unsqueeze(0), contour.unsqueeze(0)), dim=0)

    # Measuring only computation time.
    start = time.time()

    model = UNet(data.shape, data.shape, num_classes, opt)
    model.load_state_dict(torch.load(opt.trained_model))

    # Enabling GPU.
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)
    data = data.to(device)

    # Predict cancer lesions.
    y_pred = model(data.unsqueeze(0))
    prediction = torch.argmax(y_pred.squeeze(0), dim=0)

    prediction = prediction.cpu().detach().numpy()

    prediction = fit_prediction_to_pet(old_pet_shape, pet_h, prediction, minx, maxx, miny, maxy, minz, maxz, opt)

    # Measuring end time only of computation.
    end = time.time()
    print("Computations took {:4}s".format(end - start))

    # Save prediction.
    nrrd.write(opt.prediction_name, prediction, pet_h)
    print("Saved as {}".format(opt.prediction_name))

if __name__ == "__main__":
    start = time.time()
    predict()
    end = time.time()
    print("Completed in {:4}s".format(end - start))